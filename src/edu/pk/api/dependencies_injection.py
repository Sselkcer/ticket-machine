from injector import singleton

from src.edu.pk.api.ticket_facade import TicketFacade
from src.edu.pk.api.ticket_service import TicketService
from src.edu.pk.api.model.database_base import DatabaseBase
from src.edu.pk.api.model.mongo_database import MongoDatabase


def configure(binder):
    """
    Configure function (dependency injection pattern)
    :param binder: binder
    :return: nothing
    """
    binder.bind(TicketService, to=TicketService, scope=singleton)
    binder.bind(TicketFacade, to=TicketFacade, scope=singleton)
    binder.bind(DatabaseBase, to=MongoDatabase, scope=singleton)

from injector import inject

from src.edu.pk.api.ticket_service import TicketService


class TicketFacade:
    """
    Ticket Facade class (facade pattern)
    """
    @inject
    def __init__(self, service: TicketService):
        """
        The constructor initializes the fields (dependency injection pattern)
        :param service: ticket service
        """
        self.service = service

    def get_all_tickets(self):
        """
        Get all tickets from database
        :return: method returning all tickets
        """
        return self.service.get_all_tickets()

    def get_ticket_by_id(self, _id):
        """
        Get all ticket by id from database
        :param _id: ticket id
        :return: method returning all tickets by id
        """
        return self.service.get_ticket_by_id(_id)

    def get_tickets_by_kind_and_zone(self, kind, zone):
        """
        Get all tickets by kind and zone
        :param kind: kind of ticket
        :param zone: zone of ticket
        :return: method returning all tickets by kind and zone
        """
        return self.service.get_tickets_by_kind_and_zone(kind, zone)

    def add_ticket(self, request):
        """
        Insert ticket to database
        :param request: body (json)
        :return: method inserting a ticket into the database
        """
        return self.service.add_ticket(request)

    def remove_ticket(self, _id):
        """
        Remove ticket from database
        :param _id: ticket id
        :return: method removing a ticket from database
        """
        return self.service.remove_ticket(_id)

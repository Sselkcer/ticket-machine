from flask import Flask
from flask_pymongo import PyMongo

from src.edu.pk.api.model.database_base import DatabaseBase

app = Flask(__name__)


class MongoDatabase(DatabaseBase):
    """
    Class enabling connection and communication with the mongodb database
    """

    __connection_status = None

    def __init__(self):
        """
        The constructor
        """
        super().__init__()
        self.__connection_status = self.connect()

    def connect(self):
        """
        Connects to the database
        :return: nothing
        """
        app.config['MONGO_URI'] = 'mongodb://localhost:27017/ticket_machine'
        mongo = PyMongo(app)
        self.repository = mongo.db.ticket
        print("Successfully connected to MongoDb model!")
        return True

    def get_all_tickets(self):
        """
        Get all tickets from database
        :return: find() on database
        """
        return self.repository.find()

    def get_ticket_by_id(self, _id):
        """
        Get all tickets by id
        :param _id: ticket id
        :return: find_one() on database
        """
        return self.repository.find_one({'_id': _id})

    def get_tickets_by_kind_and_zone(self, kind, zone):
        """
        Get all tickets by kind and zone
        :param kind: kind of ticket
        :param zone: zone of ticket
        :return: find() on database
        """
        return self.repository.find({'$and': [{'kind': kind}, {'zone': zone}]})

    def add_ticket(self, ticket):
        """
        Insert ticket to database
        :param ticket: ticket body
        :return: insert() on database
        """
        return self.repository.insert(ticket)

    def remove_ticket(self, _id):
        """
        Remove ticket from database
        :param _id: ticket id
        :return: remove() on database
        """
        self.repository.remove({'_id': _id})

    def get_connection_status(self):
        """
        Get connection status
        :return: status
        """
        return self.__connection_status

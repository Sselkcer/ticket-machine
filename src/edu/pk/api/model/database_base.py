from abc import ABC, abstractmethod


class DatabaseBase(ABC):
    """
    Database class
    """
    def __init__(self):
        self.repository = None

    @abstractmethod
    def connect(self):
        """
        Connect depends on implementation
        :return: depends on implementation
        """
        pass

    @abstractmethod
    def get_all_tickets(self):
        """
        Gets all ticket
        :return: depends on implementation
        """
        pass

    @abstractmethod
    def get_ticket_by_id(self, _id):
        """
        Gets ticket by id
        :param _id: ticket id
        :return: depends on implementation
        """
        pass

    @abstractmethod
    def get_tickets_by_kind_and_zone(self, kind, zone):
        """
        Gets tickets by kind and zone
        :param kind: kind of ticket
        :param zone: zone of ticket
        :return: depends on implementation
        """
        pass

    @abstractmethod
    def add_ticket(self, ticket):
        """
        Inserts ticket
        :param ticket: ticket body
        :return: depends on implementation
        """
        pass

    @abstractmethod
    def remove_ticket(self, _id):
        """
        Removes ticket
        :param _id: ticket id
        :return: depends on implementation
        """
        pass

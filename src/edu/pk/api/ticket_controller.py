from flask import request
from flask_injector import FlaskInjector
from injector import inject

from src.edu.pk.api.ticket_facade import TicketFacade
from src.edu.pk.api.model.mongo_database import app
from src.edu.pk.api.dependencies_injection import configure


@inject
@app.route('/ticket', methods=['GET'])
def get_all_tickets(service: TicketFacade):
    """
    Get all tickets from database
    :param service: ticket facade
    :return: method returning all tickets
    """
    return service.get_all_tickets()


@app.route('/ticket/<int:_id>', methods=['GET'])
def get_ticket_by_id(_id, service: TicketFacade):
    """

    Get all ticket by id from database
    :param _id: ticket id
    :param service: ticket facade
    :return: method returning all tickets by id
    """
    return service.get_ticket_by_id(_id)


@app.route('/ticket/<kind>/<int:zone>', methods=['GET'])
def get_tickets_by_kind_and_zone(kind, zone, service: TicketFacade):
    """
    Get all tickets by kind and zone
    :param kind: kind of ticket
    :param zone: zone of ticket
    :param service: ticket facade
    :return: method returning all tickets by kind and zone
    """
    return service.get_tickets_by_kind_and_zone(kind, zone)


@app.route('/ticket', methods=['POST'])
def add_ticket(service: TicketFacade):
    """
    Insert ticket to database
    :param service: ticket facade
    :return: method inserting a ticket into the database
    """
    return service.add_ticket(request)


@app.route('/ticket/<int:_id>', methods=['DELETE'])
def remove_ticket(_id, service: TicketFacade):
    """
    Remove ticket from database
    :param _id: ticket id
    :param service: ticket facade
    :return: method removing a ticket from database
    """
    return service.remove_ticket(_id)


FlaskInjector(app=app, modules=[configure])

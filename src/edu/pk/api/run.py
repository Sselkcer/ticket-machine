from src.edu.pk.api.ticket_controller import app

if __name__ == '__main__':
    """
    Startup file of ticket controller class
    """
    app.run(debug=True)

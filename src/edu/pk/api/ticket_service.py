from flask import jsonify
from injector import inject

from src.edu.pk.api.model.database_base import DatabaseBase


class TicketService:
    """
    Ticket service
    """

    @inject
    def __init__(self, database: DatabaseBase):
        """
        The constructor initializes the fields (dependency injection pattern)
        :param database: database
        """
        self.repository = database
        print(f"DatabaseBase instance is {database}")

    def get_all_tickets(self):
        """
        Get all tickets from database
        :return: all tickets
        """
        output = []

        for ticket in self.repository.get_all_tickets():
            output.append(
                {'_id': ticket['_id'], 'kind': ticket['kind'], 'zone': ticket['zone'], 'price': ticket['price'],
                 'ticket_type': ticket['ticket_type']})

        return jsonify(output)

    def get_ticket_by_id(self, _id):
        """
        Get all ticket by id from database
        :param _id: ticket id
        :return: all tickets by id
        """
        ticket_by_id = self.repository.get_ticket_by_id(_id)
        return jsonify(ticket_by_id)

    def get_tickets_by_kind_and_zone(self, kind, zone):
        """
        Get all tickets by kind and zone
        :param kind: kind of ticket
        :param zone: zone of ticket
        :return: all tickets by kind and zone
        """
        ticket_by_kind_and_zone = self.repository.get_tickets_by_kind_and_zone(kind, zone)
        output = []

        for ticket in ticket_by_kind_and_zone:
            output.append(
                {'_id': ticket['_id'], 'kind': ticket['kind'], 'zone': ticket['zone'], 'price': ticket['price'],
                 'ticket_type': ticket['ticket_type']})

        return jsonify(output)

    def add_ticket(self, request):
        """
        Insert ticket to database
        :param request: body (json)
        :return: body
        """
        _id = request.json['_id']
        kind = request.json['kind']
        zone = request.json['zone']
        price = request.json['price']
        ticket_type = request.json['ticket_type']

        self.repository.add_ticket({'_id': _id, 'kind': kind, 'zone': zone, 'price': price, 'ticket_type': ticket_type})

        ticket = self.repository.get_ticket_by_id(_id)
        output = {'_id': ticket['_id'], 'kind': ticket['kind'], 'zone': ticket['zone'], 'price': ticket['price'],
                  'ticket_type': ticket['ticket_type']}

        return jsonify(output)

    def remove_ticket(self, _id):
        """
        Remove ticket from database
        :param _id: ticket id
        :return: all tickets
        """
        self.repository.remove_ticket(_id)
        return self.get_all_tickets()

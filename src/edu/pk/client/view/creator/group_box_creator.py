from PyQt5 import QtCore

from src.edu.pk.client.view.constants.group_box_enum import GroupBoxType
from src.edu.pk.client.view.model.group_box import GroupBox

group_boxes = []


class GroupBoxCreator:
    """
    Creator class for creating group box
    """
    central_widget = None

    def __init__(self, central_widget):
        """
        The constructor initializes the fields
        :param central_widget: central widget
        """
        self.central_widget = central_widget
        self.banknote_group_box = None
        self.coins_group_box = None
        self.ticket_group_box_ticket = None
        self.ticket_group_box_type = None
        self.ticket_group_box_zone = None
        self.ticket_group_box_sum_up = None
        self.ticket_group_box_count = None
        self.ticket_group_box_finish = None

    def get_group_box(self, object_name):
        """
        Create group box if not exist otherwise return created groupbox (flyweight pattern)
        :param object_name:
        :return: group box
        """
        if object_name == GroupBoxType.banknote_group_box:
            if self.banknote_group_box is None:
                self.banknote_group_box = GroupBox(self.central_widget, QtCore.QRect(85, 30, 310, 70),
                                                   "banknote_group_box")
                group_boxes.append(self.banknote_group_box)
            return self.banknote_group_box

        if object_name == GroupBoxType.coins_group_box:
            if self.coins_group_box is None:
                self.coins_group_box = GroupBox(self.central_widget, QtCore.QRect(60, 30, 360, 70),
                                                "coins_group_box")
                group_boxes.append(self.coins_group_box)
            return self.coins_group_box

        if object_name == GroupBoxType.ticket_group_box_ticket:
            if self.ticket_group_box_ticket is None:
                self.ticket_group_box_ticket = GroupBox(self.central_widget, QtCore.QRect(120, 110, 231, 251),
                                                        "ticket_group_box_ticket",
                                                        style="background-color: rgb(42, 57, 144)")
                group_boxes.append(self.ticket_group_box_ticket)
            return self.ticket_group_box_ticket

        if object_name == GroupBoxType.ticket_group_box_type:
            if self.ticket_group_box_type is None:
                self.ticket_group_box_type = GroupBox(self.central_widget, QtCore.QRect(430, 30, 231, 251),
                                                      "ticket_group_box_type",
                                                      style="background-color: rgb(42, 57, 144)")
                group_boxes.append(self.ticket_group_box_type)
            return self.ticket_group_box_type

        if object_name == GroupBoxType.ticket_group_box_zone:
            if self.ticket_group_box_zone is None:
                self.ticket_group_box_zone = GroupBox(self.central_widget, QtCore.QRect(430, 300, 231, 251),
                                                      "ticket_group_box_zone",
                                                      style="background-color: rgb(42, 57, 144)")
                group_boxes.append(self.ticket_group_box_zone)
            return self.ticket_group_box_zone

        if object_name == GroupBoxType.ticket_group_box_sum_up:
            if self.ticket_group_box_sum_up is None:
                self.ticket_group_box_sum_up = GroupBox(self.central_widget, QtCore.QRect(430, 300, 231, 251),
                                                        "ticket_group_box_sum_up",
                                                        style="background-color: rgb(42, 57, 144)")
                group_boxes.append(self.ticket_group_box_sum_up)
            return self.ticket_group_box_sum_up

        if object_name == GroupBoxType.ticket_group_box_count:
            if self.ticket_group_box_count is None:
                self.ticket_group_box_count = GroupBox(self.central_widget, QtCore.QRect(430, 300, 231, 251),
                                                       "ticket_group_box_count",
                                                       style="background-color: rgb(42, 57, 144)")
                group_boxes.append(self.ticket_group_box_count)
            return self.ticket_group_box_count

        if object_name == GroupBoxType.ticket_group_box_finish:
            if self.ticket_group_box_finish is None:
                self.ticket_group_box_finish = GroupBox(self.central_widget, QtCore.QRect(430, 580, 231, 251),
                                                        "ticket_group_box_finish",
                                                        style="background-color: rgb(42, 57, 144)")
                group_boxes.append(self.ticket_group_box_finish)
            return self.ticket_group_box_finish

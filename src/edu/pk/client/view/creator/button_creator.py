from src.edu.pk.client.view.constants.group_box_enum import GroupBoxType
from src.edu.pk.client.view.model.button import Button


class ButtonCreator:
    """
    Creator class for creating buttons
    """

    def __init__(self, central_widget, group_box_factory):
        """
        The constructor initializes the fields
        :param central_widget: central widget
        :param group_box_factory: group box factory
        """
        self.central_widget = central_widget
        self.central_widget.setObjectName("central_widget")

        self.banknote_group_box = group_box_factory.get_group_box(GroupBoxType.banknote_group_box).get_widget()
        self.coins_group_box = group_box_factory.get_group_box(GroupBoxType.coins_group_box).get_widget()
        self.ticket_group_box_ticket = group_box_factory.get_group_box(
            GroupBoxType.ticket_group_box_ticket).get_widget()
        self.ticket_group_box_type = group_box_factory.get_group_box(GroupBoxType.ticket_group_box_type).get_widget()
        self.ticket_group_box_zone = group_box_factory.get_group_box(GroupBoxType.ticket_group_box_zone).get_widget()
        self.ticket_group_box_finish = group_box_factory.get_group_box(
            GroupBoxType.ticket_group_box_finish).get_widget()

    def create_banknote_button(self, geometry, object_name, text):
        """
        Create button inside banknote_group_box
        :param geometry: position and sizes of
        :param object_name: object name to be set to widget
        :param text: widget text
        :return: button wrapper object
        """
        return Button(self.banknote_group_box, geometry, object_name, text)

    def create_coin_button(self, geometry, object_name, text):
        """
        Create button inside coins group box
        :param geometry: position and sizes of
        :param object_name: object name to be set to widget
        :param text: widget text
        :return: button wrapper object
        """
        return Button(self.coins_group_box, geometry, object_name, text)

    def create_open_dialog_button(self, geometry, object_name, text):
        """
        Create button inside central widget
        :param geometry: position and sizes of
        :param object_name: object name to be set to widget
        :param text: widget text
        :return: button wrapper object
        """
        return Button(self.central_widget, geometry, object_name, text)

    def create_static_button(self, geometry, object_name):
        """
        Create button inside central widget
        :param geometry: position and sizes of
        :param object_name: object name to be set to widget
        :return: button wrapper object
        """
        return Button(self.central_widget, geometry, object_name, style="background-color: rgb(210, 51, 105)")

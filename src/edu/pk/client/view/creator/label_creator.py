from src.edu.pk.client.view.constants.group_box_enum import GroupBoxType
from src.edu.pk.client.view.model.label import Label


class LabelCreator:
    """
    Creator class for creating labels
    """

    def __init__(self, central_widget, group_box_factory):
        """
        The constructor initializes the fields
        :param central_widget: central widget
        :param group_box_factory: group box factory
        """
        self.central_widget = central_widget
        self.central_widget.setObjectName("central_widget")

        self.banknote_group_box = group_box_factory.get_group_box(GroupBoxType.banknote_group_box).get_widget()
        self.coins_group_box = group_box_factory.get_group_box(GroupBoxType.coins_group_box).get_widget()
        self.ticket_group_box_ticket = group_box_factory.get_group_box(
            GroupBoxType.ticket_group_box_ticket).get_widget()
        self.ticket_group_box_type = group_box_factory.get_group_box(GroupBoxType.ticket_group_box_type).get_widget()
        self.ticket_group_box_zone = group_box_factory.get_group_box(GroupBoxType.ticket_group_box_zone).get_widget()
        self.ticket_group_box_count = group_box_factory.get_group_box(GroupBoxType.ticket_group_box_count).get_widget()
        self.ticket_group_box_sum_up = group_box_factory.get_group_box(
            GroupBoxType.ticket_group_box_sum_up).get_widget()
        self.ticket_group_box_finish = group_box_factory.get_group_box(
            GroupBoxType.ticket_group_box_finish).get_widget()

    def create_main_view_label(self, geometry, object_name, text, style):
        """
        Create label inside main view
        :param geometry: position and sizes of
        :param object_name: object name to be set to widget
        :param text: widget text
        :param style: widget css styles
        :return: label wrapper object
        """
        return Label(self.ticket_group_box_ticket, geometry, object_name, text, style)

    def create_ticket_type_label(self, geometry, object_name, text):
        """
        Create label inside ticket type screen
        :param geometry: position and sizes of
        :param object_name: object name to be set to widget
        :param text: widget text
        :return: label wrapper object
        """
        return Label(self.ticket_group_box_type, geometry, object_name, text)

    def create_ticket_zone_label(self, geometry, object_name, text):
        """
        Create label inside ticket zone screen
        :param geometry: position and sizes of
        :param object_name: object name to be set to widget
        :param text: widget text
        :return: abel wrapper object
        """
        return Label(self.ticket_group_box_zone, geometry, object_name, text)

    def create_ticket_count_label(self, geometry, object_name, text, style):
        """
        Create label inside ticket count screen
        :param geometry: position and sizes of
        :param object_name: object name to be set to widget
        :param text: widget text
        :param style: widget css styles
        :return: label wrapper object
        """
        return Label(self.ticket_group_box_count, geometry, object_name, text, style)

    def create_sum_up_label(self, geometry, object_name, text, style):
        """
        Create label inside ticket sum up screen
        :param geometry: position and sizes of
        :param object_name: object name to be set to widget
        :param text: widget text
        :param style: widget css styles
        :return: label wrapper object
        """
        return Label(self.ticket_group_box_sum_up, geometry, object_name, text, style)

    def create_ticket_printed_label(self, geometry, object_name, text, style):
        """
        Create label inside ticket printed screen
        :param geometry: position and sizes of
        :param object_name: object name to be set to widget
        :param text: widget text
        :param style: widget css styles
        :return: label wrapper object
        """
        return Label(self.ticket_group_box_finish, geometry, object_name, text, style)

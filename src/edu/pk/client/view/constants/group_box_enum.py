from enum import Enum

GroupBoxType = Enum('GroupBoxType', 'banknote_group_box coins_group_box ticket_group_box_ticket '
                                    'ticket_group_box_type ticket_group_box_zone ticket_group_box_count '
                                    'ticket_group_box_sum_up ticket_group_box_finish')

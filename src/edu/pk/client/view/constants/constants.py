from PyQt5 import QtCore

static_buttons_geometries = [
    {"geometry": QtCore.QRect(70, 120, 41, 41), "object_name": "button_1"},
    {"geometry": QtCore.QRect(70, 170, 41, 41), "object_name": "button_2"},
    {"geometry": QtCore.QRect(70, 220, 41, 41), "object_name": "button_3"},
    {"geometry": QtCore.QRect(70, 270, 41, 41), "object_name": "button_4"},
    {"geometry": QtCore.QRect(70, 320, 41, 41), "object_name": "button_5"},
    {"geometry": QtCore.QRect(360, 120, 41, 41), "object_name": "button_6"},
    {"geometry": QtCore.QRect(360, 170, 41, 41), "object_name": "button_7"},
    {"geometry": QtCore.QRect(360, 220, 41, 41), "object_name": "button_8"},
    {"geometry": QtCore.QRect(360, 270, 41, 41), "object_name": "button_9"},
    {"geometry": QtCore.QRect(360, 320, 41, 41), "object_name": "button_10"},
]

static_back_button = {"geometry": QtCore.QRect(350, 50, 100, 51), "object_name": "back_button"}

banknotes_buttons = [
    {"geometry": QtCore.QRect(10, 20, 41, 41), "object_name": "button_10zl", "text": "10zł"},
    {"geometry": QtCore.QRect(60, 20, 41, 41), "object_name": "button_20zl", "text": "20zł"},
    {"geometry": QtCore.QRect(110, 20, 41, 41), "object_name": "button_50zl", "text": "50zł"},
    {"geometry": QtCore.QRect(260, 20, 41, 41), "object_name": "button_100zl", "text": "100zł"},
    {"geometry": QtCore.QRect(160, 20, 41, 41), "object_name": "button_200zl", "text": "200zł"},
    {"geometry": QtCore.QRect(210, 20, 41, 41), "object_name": "button_500zl", "text": "500zł"},
]

coins_buttons = [
    {"geometry": QtCore.QRect(10, 20, 41, 41), "object_name": "button_5gr", "text": "5gr"},
    {"geometry": QtCore.QRect(60, 20, 41, 41), "object_name": "button_10gr", "text": "10gr"},
    {"geometry": QtCore.QRect(110, 20, 41, 41), "object_name": "button_20gr", "text": "20gr"},
    {"geometry": QtCore.QRect(260, 20, 41, 41), "object_name": "button_50gr", "text": "50gr"},
    {"geometry": QtCore.QRect(160, 20, 41, 41), "object_name": "button_1zl", "text": "1zł"},
    {"geometry": QtCore.QRect(210, 20, 41, 41), "object_name": "button_2zl", "text": "2zł"},
    {"geometry": QtCore.QRect(310, 20, 41, 41), "object_name": "button_5zl", "text": "5zł"},
]

ticket_labels = [
    {"geometry": QtCore.QRect(0, 5, 100, 46), "object_name": "label_1", "text": "Bilet 1", "style": "color: white"},
    {"geometry": QtCore.QRect(0, 55, 100, 46), "object_name": "label_2", "text": "Bilet 2", "style": "color: white"},
    {"geometry": QtCore.QRect(0, 105, 100, 46), "object_name": "label_3", "text": "Bilet 3", "style": "color: white"},
    {"geometry": QtCore.QRect(0, 155, 100, 46), "object_name": "label_4", "text": "Bilet 4", "style": "color: white"},
    {"geometry": QtCore.QRect(0, 205, 100, 46), "object_name": "label_5", "text": "Bilet 5", "style": "color: white"},
    {"geometry": QtCore.QRect(120, 5, 100, 46), "object_name": "label_6", "text": "Bilet 6", "style": "color: white"},
    {"geometry": QtCore.QRect(120, 55, 100, 46), "object_name": "label_7", "text": "Bilet 7", "style": "color: white"},
    {"geometry": QtCore.QRect(120, 105, 100, 46), "object_name": "label_8", "text": "Bilet 8", "style": "color: white"},
    {"geometry": QtCore.QRect(120, 155, 100, 46), "object_name": "label_9", "text": "Bilet 9", "style": "color: white"},
    {"geometry": QtCore.QRect(120, 205, 100, 46), "object_name": "label_10", "text": "Bilet 10", "style": "color: white"},
]

ticket_type_labels = [
    {"geometry": QtCore.QRect(30, 50, 171, 41), "object_name": "label_ticket_type", "text": "Wybierz typ biletu"},
    {"geometry": QtCore.QRect(20, 110, 60, 41), "object_name": "label_student", "text": "Ulgowe"},
    {"geometry": QtCore.QRect(140, 110, 60, 41), "object_name": "label_normal", "text": "Normalne"},
]

ticket_zone_labels = [
    {"geometry": QtCore.QRect(30, 50, 171, 41), "object_name": "label_zone1", "text": "Wybierz strefę"},
    {"geometry": QtCore.QRect(20, 110, 60, 41), "object_name": "label_zone2", "text": "Strefa I"},
    {"geometry": QtCore.QRect(140, 110, 60, 41), "object_name": "label_zone", "text": "Strefa I+II"},
]

ticket_count_labels = [
    {"geometry": QtCore.QRect(30, 50, 171, 41), "object_name": "label_count_text",
     "text": "Ile biletów chcesz zakupić?",
     "style": "color: rgb(255, 255, 255);"},
    {"geometry": QtCore.QRect(20, 110, 60, 41), "object_name": "label_plus", "text": "+",
     "style": "font-size : 16px; color: rgb(255, 255, 255);"},
    {"geometry": QtCore.QRect(140, 110, 60, 41), "object_name": "label_minus", "text": "-",
     "style": "font-size : 16px; color: rgb(255, 255, 255);"},
    {"geometry": QtCore.QRect(80, 110, 60, 41), "object_name": "label_current", "text": "1",
     "style": "font-size : 16px; color: rgb(255, 255, 255);"},
    {"geometry": QtCore.QRect(150, 210, 71, 41), "object_name": "label_next", "text": "Przejdź dalej",
     "style": "color: rgb(255, 255, 255);"}
]

ticket_sum_up_labels = [
    {"geometry": QtCore.QRect(20, 50, 191, 41), "object_name": "label_info_sum_up",
     "text": "Podsumowanie kupionych biletów", "style": "color: rgb(255, 255, 255);"},
    {"geometry": QtCore.QRect(110, 90, 80, 41), "object_name": "label_total_cost", "text": "0,00zł",
     "style": "font-size : 16px; color: rgb(255, 255, 255);"},
    {"geometry": QtCore.QRect(30, 90, 91, 41), "object_name": "label_to_pay", "text": "Do zapłaty",
     "style": "color: rgb(255, 255, 255);"},
    {"geometry": QtCore.QRect(60, 130, 101, 80), "object_name": "label_bought_tickets",
     "text": "Info o biletach\n Info o biletach\n Info o biletach\n Info o biletach\n",
     "style": "color: rgb(255, 255, 255);"}
]

ticket_printed_labels = [
    {"geometry": QtCore.QRect(30, 10, 171, 15), "object_name": "label_ticket_printed", "text": "Wydrukowano bilety:",
     "style": "color: rgb(255, 255, 255)"},
    {"geometry": QtCore.QRect(30, 35, 171, 170), "object_name": "label_ticket_printed_info",
     "text": "Bilet ulgowy 2.30zł", "style": "color: rgb(255, 255, 255); background-color: rgb(225, 11, 47)"},
    {"geometry": QtCore.QRect(30, 215, 171, 15), "object_name": "label_ticket_rest", "text": "Reszta:",
     "style": "color: rgb(255, 255, 255)"},
]

ticket_open_dialog_buttons = [
    {"geometry": QtCore.QRect(140, 60, 91, 31), "object_name": "button_coins", "text": "Włóż monety"},
    {"geometry": QtCore.QRect(240, 60, 91, 31), "object_name": "button_banknotes", "text": "Włóż banknoty"}
]

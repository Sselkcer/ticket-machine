from enum import Enum

TicketBuyStatus = Enum('TicketBuyStatus', 'type zone ticket count sum_up finish')

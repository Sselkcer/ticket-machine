from PyQt5 import QtWidgets

from src.edu.pk.client.view.constants.constants import static_buttons_geometries, banknotes_buttons, coins_buttons, \
    ticket_labels, ticket_type_labels, ticket_zone_labels, ticket_printed_labels, ticket_open_dialog_buttons, \
    ticket_count_labels, ticket_sum_up_labels, static_back_button
from src.edu.pk.client.view.creator.button_creator import ButtonCreator
from src.edu.pk.client.view.creator.group_box_creator import GroupBoxCreator
from src.edu.pk.client.view.creator.label_creator import LabelCreator

buttons_clickable = []
labels = []


class ViewBuilder:
    """
    Class used for building view
    """
    def __init__(self):
        """
        The constructor initializes the fields
        """
        self.main_window = QtWidgets.QMainWindow()
        self.central_widget = QtWidgets.QWidget(self.main_window)

        self.group_box_factory = GroupBoxCreator(self.central_widget)
        self.button_factory = ButtonCreator(self.central_widget, self.group_box_factory)
        self.label_factory = LabelCreator(self.central_widget, self.group_box_factory)

    def set_up_ui(self):
        """
        Set elements inside main window
        :return: nothing
        """
        self.main_window.resize(500, 500)
        self.main_window.setCentralWidget(self.central_widget)

        self.append_static_buttons_geometries()
        self.append_static_back_button()
        self.append_banknotes_buttons()
        self.append_coins_buttons()
        self.append_ticket_open_dialog_buttons()

        self.create_main_view_labels()
        self.create_ticket_type_labels()
        self.create_ticket_zone_labels()
        self.create_ticket_printed_labels()

        self.append_ticket_count_labels()
        self.append_ticket_sum_up_labels()

    def append_static_buttons_geometries(self):
        """
        Create static buttons from constants
        :return: added buttons
        """
        return [buttons_clickable.append(
            self.button_factory.create_static_button(button["geometry"], button["object_name"])) for button in
            static_buttons_geometries]

    def append_static_back_button(self):
        """
        Create static buttons from constants
        :return: added buttons
        """
        return buttons_clickable.append(
            self.button_factory.create_static_button(static_back_button["geometry"], static_back_button["object_name"]))

    def append_banknotes_buttons(self):
        """
        Create banknote buttons from constants
        :return: added buttons
        """
        return [buttons_clickable.append(
            self.button_factory.create_banknote_button(button["geometry"], button["object_name"], button["text"])) for
            button in banknotes_buttons]

    def append_coins_buttons(self):
        """
        Create coin buttons from constants
        :return: added buttons
        """
        return [buttons_clickable.append(
            self.button_factory.create_coin_button(button["geometry"], button["object_name"], button["text"])) for
            button in coins_buttons]

    def append_ticket_open_dialog_buttons(self):
        """
        Create open dialog buttons from constants
        :return: added buttons
        """
        return [buttons_clickable.append(
            self.button_factory.create_open_dialog_button(button["geometry"], button["object_name"], button["text"]))
            for button in ticket_open_dialog_buttons]

    def create_main_view_labels(self):
        """
        Create main view labels from constants
        :return: added labels
        """
        return [labels.append(
            self.label_factory.create_main_view_label(label["geometry"], label["object_name"], label["text"],
                                                      label["style"])) for label in ticket_labels]

    def create_ticket_type_labels(self):
        """
        Create ticket type labels from constants
        :return: added labels
        """
        return [self.label_factory.create_ticket_type_label(label["geometry"], label["object_name"], label["text"]) for
                label in ticket_type_labels]

    def create_ticket_zone_labels(self):
        """
        Create ticket type labels from constants
        :return: added labels
        """
        return [self.label_factory.create_ticket_zone_label(label["geometry"], label["object_name"], label["text"]) for
                label in ticket_zone_labels]

    def create_ticket_printed_labels(self):
        """
        Create ticket type labels from constants
        :return: added labels
        """
        return [labels.append(
            self.label_factory.create_ticket_printed_label(label["geometry"], label["object_name"], label["text"],
                                                           label["style"])) for label in ticket_printed_labels]

    def append_ticket_count_labels(self):
        """
        Create ticket type labels from constants
        :return: added labels
        """
        return [labels.append(
            self.label_factory.create_ticket_count_label(label["geometry"], label["object_name"], label["text"],
                                                         label["style"])) for label in ticket_count_labels]

    def append_ticket_sum_up_labels(self):
        """
        Create ticket type labels from constants
        :return: added labels
        """
        return [labels.append(
            self.label_factory.create_sum_up_label(label["geometry"], label["object_name"], label["text"],
                                                   label["style"])) for label in ticket_sum_up_labels]

    def show_ui(self):
        """
        Show builded main window
        :return: nothing
        """
        self.main_window.show()

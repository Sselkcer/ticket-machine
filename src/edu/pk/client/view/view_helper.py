from src.edu.pk.client.view.creator.group_box_creator import group_boxes
from src.edu.pk.client.view.view_builder import buttons_clickable, labels


class ViewHelper:
    """
    Class used to find widgets already added to view
    """
    @staticmethod
    def find_widget_by_object_name(object_name):
        """
        Find widget with passed object_name across all added to view
        :param object_name: widget object_name to find
        :return: widget with passed object_name
        """
        return list(filter(lambda widget: widget.get_widget().objectName() == object_name,
                           (buttons_clickable + labels + group_boxes)))[0].get_widget()

    @staticmethod
    def find_button_clickable_by_object_name(object_name):
        """
        Find button with passed object_name across all added to view
        :param object_name: widget object_name to find
        :return: widget with passed object_name
        """
        return list(filter(lambda button: button.button.objectName() == object_name, buttons_clickable))[0].button

    @staticmethod
    def find_label_by_object_name(object_name):
        """
        Find label with passed object_name across all added to view
        :param object_name: widget object_name to find
        :return: widget with passed object_name
        """
        return list(filter(lambda label: label.label.objectName() == object_name, labels))[0].label

    @staticmethod
    def get_all_static_buttons():
        """
        Get all buttons able to click
        :return: all buttons able to click
        """
        return buttons_clickable

    @staticmethod
    def find_group_box_by_object_name(object_name):
        """
        Find group box with passed object_name
        :param object_name: widget object_name to find
        :return: widget with passed object_name
        """
        return list(filter(lambda group_box: group_box.get_widget().objectName() == object_name, group_boxes))[
            0].get_widget()

    @staticmethod
    def hide_elements(elements):
        """
        Hide widgets
        :param elements: widgets to hide
        :return: hidden widgets
        """
        return [element.setVisible(False) for element in elements]


class EagerHandlerValueBinder:
    """
    Helper class that bind event to widget inside loop to force eager loading
    """
    def __init__(self, ticket_id, button, handler):
        """
        The constructor initializes the fields
        :param ticket_id: ticket id
        :param button: button
        :param handler: handler function
        """
        self.ticket_id = ticket_id
        self.button = button
        self.button.mouseReleaseEvent = lambda event: handler(ticket_id)

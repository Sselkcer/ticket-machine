from src.edu.pk.client.view.creator.group_box_creator import group_boxes
from src.edu.pk.client.view.view_helper import ViewHelper


def hide_groupbox_widgets(f):
    """
    Hide all groupboxes (decorator pattern)
    :param f: function to decorate
    :return: wrapper function
    """
    def wrapper(self, *args):
        for widget in group_boxes:
            widget.get_widget().setVisible(False)
        return f(self, *args)

    return wrapper


def show_widgets(widgets):
    """
    Show widgets passed as arguments (decorator pattern)
    :param widgets: array of widgets object names
    :return: wrapper function
    """
    def _show_widgets(f):
        def wrapper(self, *args):
            for widget_object_name in widgets:
                ViewHelper.find_widget_by_object_name(widget_object_name).setVisible(True)
            return f(self, *args)

        return wrapper

    return _show_widgets


def hide_widgets(widgets):
    """
    Hide widgets passed as arguments (decorator pattern)
    :param widgets: array of widgets object names
    :return: wrapper function
    """
    def _hide_widgets(f):
        def wrapper(self, *args):
            for widget_object_name in widgets:
                ViewHelper.find_widget_by_object_name(widget_object_name).setVisible(False)
            return f(self, *args)

        return wrapper

    return _hide_widgets

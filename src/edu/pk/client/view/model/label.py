from PyQt5 import QtWidgets, QtCore

from src.edu.pk.client.view.model.base_widget import BaseWidget


class Label(BaseWidget):
    """
    Wrapper class for QLabel
    """
    def __init__(self, parent, geometry, object_name, text="", style="color: rgb(255, 255, 255)",
                 alignment=QtCore.Qt.AlignCenter, auto_fill_background=True):
        """
        The constructor initializes the fields
        :param parent: parent
        :param geometry: place where the object should be located
        :param object_name: object name
        :param text: hint
        :param style: css style
        :param alignment: alignment
        :param auto_fill_background: auto fill background
        """
        self.label = QtWidgets.QLabel(parent)
        self.label.setGeometry(geometry)
        self.label.setObjectName(object_name)
        self.label.setAutoFillBackground(auto_fill_background)
        self.label.setStyleSheet(style)
        self.label.setText(text)
        self.label.setAlignment(alignment)

    def get_widget(self):
        """
        Get widget
        :return qt widget group_box label
        """
        return self.label

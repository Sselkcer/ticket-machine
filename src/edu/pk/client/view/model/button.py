from PyQt5 import QtWidgets

from src.edu.pk.client.view.model.base_widget import BaseWidget


class Button(BaseWidget):
    """
    Wrapper class for QPushButton
    """
    def __init__(self, parent, geometry, object_name, text="", style="", auto_fill_background=True):
        """
        The constructor initializes the fields
        :param parent: parent
        :param geometry: place where the object should be located
        :param object_name: object name
        :param text: hint
        :param style: css style
        :param auto_fill_background: auto fill background
        """
        self.button = QtWidgets.QPushButton(parent)
        self.button.setGeometry(geometry)
        self.button.setObjectName(object_name)
        self.button.setAutoFillBackground(auto_fill_background)
        self.button.setStyleSheet(style)
        self.button.setText(text)

    def get_widget(self):
        """
        Get widget
        :return qt widget object button
        """
        return self.button

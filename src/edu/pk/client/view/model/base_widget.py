from abc import ABC, abstractmethod


class BaseWidget(ABC):
    """
    Base widget interface class for widgets wrappers
    """
    @abstractmethod
    def get_widget(self):
        """
        Get widget
        :return qt widget object
        """
        pass

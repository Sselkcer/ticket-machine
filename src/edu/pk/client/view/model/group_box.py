from PyQt5 import QtWidgets

from src.edu.pk.client.view.model.base_widget import BaseWidget


class GroupBox(BaseWidget):
    """
    Wrapper class for QGroupBox
    """
    def __init__(self, parent, geometry, object_name, style=""):
        """
        The constructor initializes the fields
        :param parent: parent
        :param geometry: place where the object should be located
        :param object_name: object name
        :param style: css style
        """
        self.group_box = QtWidgets.QGroupBox(parent)
        self.group_box.setGeometry(geometry)
        self.group_box.setObjectName(object_name)
        self.group_box.setStyleSheet(style)

    def get_widget(self):
        """
        Get widget
        :return qt widget group_box button
        """
        return self.group_box

import socket

from src.edu.pk.client.patterns.singleton.singleton_type import SingletonType


class Connector(metaclass=SingletonType):
    """
    Class used to create connection to ticket machine socket (singleton pattern)
    """
    def __init__(self):
        """
        The constructor initializes the fields, opens a connection
        """
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect((socket.gethostname(), 1234))
        except ConnectionRefusedError:
            print("FAILED TO CONNECT WITH SERVER")

    def close_connection(self):
        """
        Close connection to socket
        :return: nothing
        """
        self.socket.close()

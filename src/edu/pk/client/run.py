from src.edu.pk.client.client import Client

if __name__ == '__main__':
    """
    Startup file of client class
    """
    main = Client()
    main.run_app()

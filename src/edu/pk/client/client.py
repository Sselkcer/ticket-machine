import pickle

from PyQt5 import QtWidgets
from PyQt5 import QtCore

from src.edu.pk.client.patterns.singleton.connector import Connector
from src.edu.pk.client.view.constants.constants import static_buttons_geometries
from src.edu.pk.client.view.constants.ticket_buy_status import TicketBuyStatus
from src.edu.pk.client.view.decorators import hide_widgets, show_widgets, hide_groupbox_widgets
from src.edu.pk.client.view.view_builder import ViewBuilder
from src.edu.pk.client.view.view_helper import ViewHelper, EagerHandlerValueBinder


class Client:
    """
    Client class which manages start of GUI and handles setting listeners to widgets.
    Represents client class (client-server pattern)
    """
    def __init__(self):
        """
        The constructor initializes the fields and opens a connection
        """

        self.api = Connector()
        self.view_resolver = None
        self.button3 = None
        self.button8 = None
        self.button10 = None
        self.current_amount = 1
        self.current_total_cost = None
        self.insert_coins = None
        self.insert_banknotes = None
        self.label_count = None
        self.back_button = None
        self.button_coins = None
        self.button_banknotes = None
        self.default_geometry = QtCore.QRect(120, 110, 231, 251)

    def run_app(self):
        """
        Create main view used to set initial values after loading view
        :return: nothing
        """
        import sys
        app = QtWidgets.QApplication(sys.argv)

        view = ViewBuilder()
        view.set_up_ui()
        view.show_ui()

        self.button3 = ViewHelper.find_button_clickable_by_object_name("button_3")
        self.button8 = ViewHelper.find_button_clickable_by_object_name("button_8")
        self.button10 = ViewHelper.find_button_clickable_by_object_name("button_10")
        self.label_count = ViewHelper.find_label_by_object_name("label_current")
        self.insert_coins = ViewHelper.find_button_clickable_by_object_name("button_coins")
        self.insert_banknotes = ViewHelper.find_button_clickable_by_object_name("button_banknotes")
        self.back_button = ViewHelper.find_button_clickable_by_object_name("back_button")
        self.button_coins = ViewHelper.find_button_clickable_by_object_name("button_coins")
        self.button_banknotes = ViewHelper.find_button_clickable_by_object_name("button_banknotes")
        self.back_button.mouseReleaseEvent = self.back_button_handler
        self.back_button.setText("Dodaj inny bilet")

        self.button_coins.setVisible(False)
        self.button_banknotes.setVisible(False)
        self.back_button.setVisible(False)

        kinds_of_tickets = self.api.socket.recv(1024)

        self.set_screen_by_ticket_status(TicketBuyStatus.type)
        self.set_type_button_handlers(kinds_of_tickets.decode("utf-8"))

        sys.exit(app.exec_())

    def back_button_handler(self, event):
        """
        Handler function for clicking in back button
        :param event: event passed by QT
        :return: nothing
        """
        self.set_screen_by_ticket_status(TicketBuyStatus.type, True)
        kinds_of_tickets = self.api.socket.recv(1024)
        self.set_type_button_handlers(kinds_of_tickets.decode("utf-8"))

    def set_type_button_handlers(self, kinds_of_tickets=""):
        """
        Sets handlers for clicking button during choice between full price ticket and half price ticket
        :param kinds_of_tickets: kinds of ticket
        :return: nothing
        """
        kinds = kinds_of_tickets.split("|")
        self.button3.mouseReleaseEvent = lambda event: self.type_select_handler(kinds[0])
        self.button8.mouseReleaseEvent = lambda event: self.type_select_handler(kinds[1])

    def type_select_handler(self, kind):
        """
        Send choosed type and receive available zones
        :param kind: chosen ticket type
        :return: nothing
        """
        self.api.socket.send(bytes(kind, "utf-8"))
        self.set_screen_by_ticket_status(TicketBuyStatus.zone)
        zones = self.api.socket.recv(1024)
        self.set_zone_button_handlers(zones.decode("utf-8"))

    def set_zone_button_handlers(self, zones=""):
        """
        Sets handlers for clicking buttons during choice between zone I and zone II
        :param zones: available zones
        :return: nothing
        """
        zone = zones.split("|")
        self.button3.mouseReleaseEvent = lambda event: self.zone_select_handler(zone[0])
        self.button8.mouseReleaseEvent = lambda event: self.zone_select_handler(zone[1])

    def zone_select_handler(self, zone):
        """
        Send chosen zone and receive available tickets
        :param zone: chosen zone
        :return: nothing
        """
        self.api.socket.send(bytes(zone, "utf-8"))
        self.set_screen_by_ticket_status(TicketBuyStatus.ticket)
        message = self.api.socket.recv(1024)
        tickets = pickle.loads(message)
        self.prepare_hint_for_buttons(tickets)

    def prepare_hint_for_buttons(self, tickets):
        """
        Set label text for received tickets and add proper handlers
        :param tickets: received tickets
        :return: nothing
        """
        for i, value in enumerate(static_buttons_geometries):
            button = ViewHelper.find_button_clickable_by_object_name(value["object_name"])
            try:
                EagerHandlerValueBinder(tickets[i]["_id"], button,
                                        lambda ticket_id: self.ticket_select_handler(ticket_id))

                label = ViewHelper.find_label_by_object_name("label_" + str(i + 1))
                label.setText(str(tickets[i]["price"]) + "\n" + tickets[i]["ticket_type"])
                label.setWordWrap(True)
            except:
                EagerHandlerValueBinder("", button, lambda none: print(none))
                label = ViewHelper.find_label_by_object_name("label_" + str(i + 1))
                label.setText("")

    def ticket_select_handler(self, ticket_id):
        """
        Handler function for clicking single button with ticket
        :param ticket_id: ticket id
        :return: nothing
        """
        self.api.socket.send(bytes(str(int(ticket_id)), "utf-8"))
        self.set_screen_by_ticket_status(TicketBuyStatus.count)
        self.api.socket.recv(1024)
        self.set_count_button_handlers()

    def set_count_button_handlers(self):
        """
        Sets handlers for clicking buttons during count ticket phase
        :return: nothing
        """
        self.button3.mouseReleaseEvent = self.count_add_ticket_handler
        self.button8.mouseReleaseEvent = self.count_remove_ticket_handler
        self.button10.mouseReleaseEvent = self.count_next_step_handler

    def count_add_ticket_handler(self, event):
        """
        Increase tickets count
        :param event: event passed by QT
        :return: nothing
        """
        self.current_amount += 1
        self.label_count.setText(str(self.current_amount))

    def count_remove_ticket_handler(self, event):
        """
        Decrease tickets count
        :param event: event passed by QT
        :return: nothing
        """
        if self.current_amount == 1:
            return
        self.current_amount = self.current_amount - 1
        self.label_count.setText(str(self.current_amount))

    def count_next_step_handler(self, event):
        """
        Send amount chosen and receive sum up info
        :param event: event passed by QT
        :return: nothing
        """
        self.api.socket.send(bytes(self.current_amount.__str__(), "utf-8"))
        self.set_screen_by_ticket_status(TicketBuyStatus.sum_up)
        messages = self.api.socket.recv(1024)
        message = (messages.decode("utf-8")).split("|")
        self.current_total_cost = float(message[0])
        self.prepare_hints_for_sum_up_view(message)
        self.set_sum_up_button_handlers()

    def prepare_hints_for_sum_up_view(self, message):
        """
        Set label text with message
        :param message: data from client
        :return: nothing
        """
        label_total_cost = ViewHelper.find_label_by_object_name("label_total_cost")
        label_total_cost.setText(float(message[0]).__str__())
        label_total_cost.setWordWrap(True)

        label_bought_tickets = ViewHelper.find_label_by_object_name("label_bought_tickets")
        label_bought_tickets.setText(message[1])
        label_bought_tickets.setWordWrap(True)

        label_current = ViewHelper.find_label_by_object_name("label_current")
        label_current.setText("1")
        self.current_amount = 1

    def set_sum_up_button_handlers(self):
        """
        Sets handlers for clicking buttons during pay phase
        :return: nothing
        """
        self.insert_coins.mouseReleaseEvent = self.show_coin_box
        self.insert_banknotes.mouseReleaseEvent = self.show_banknote_box

    def sum_up_next_step_handler(self):
        """
        Send info that user paid and set label with sum up info
        :return: nothing
        """
        self.api.socket.send(bytes("next", "utf-8"))
        message = self.api.socket.recv(1024).decode("utf-8")
        self.api.socket.send(bytes("rest", "utf-8"))
        rest = float(self.api.socket.recv(1024).decode("utf-8")) * -1

        label_ticket_printed = ViewHelper.find_label_by_object_name("label_ticket_printed_info")
        label_ticket_printed.setText(message)
        label_ticket_printed.setWordWrap(True)

        label_ticket_rest = ViewHelper.find_label_by_object_name("label_ticket_rest")
        label_ticket_rest.setText(f"Reszta: {rest}")

        self.set_screen_by_ticket_status(TicketBuyStatus.finish)
        self.api.close_connection()

    @hide_widgets(["button_coins", "button_banknotes", "back_button"])
    @show_widgets(["coins_group_box"])
    def show_coin_box(self, event):
        """
        Show coin box, hide overlapping buttons and sets coin handlers (decorator pattern)
        :param event: event passed by QT
        :return: nothing
        """
        self.set_coins_handlers()

    @hide_widgets(["button_coins", "button_banknotes", "back_button"])
    @show_widgets(["banknote_group_box"])
    def show_banknote_box(self, event):
        """
        Show coin box, hide overlapping buttons and sets coin handlers (decorator pattern)
        :param event: event passed by QT
        :return: nothing
        """
        self.set_banknote_handlers()

    def set_banknote_handlers(self):
        """
        Set event handlers for clicking in banknotes
        :return: nothing
        """
        ViewHelper.find_button_clickable_by_object_name("button_10zl").mouseReleaseEvent = \
            lambda event: self.insert_money(10)
        ViewHelper.find_button_clickable_by_object_name("button_20zl").mouseReleaseEvent = \
            lambda event: self.insert_money(20)
        ViewHelper.find_button_clickable_by_object_name("button_50zl").mouseReleaseEvent = \
            lambda event: self.insert_money(50)
        ViewHelper.find_button_clickable_by_object_name("button_100zl").mouseReleaseEvent = \
            lambda event: self.insert_money(100)
        ViewHelper.find_button_clickable_by_object_name("button_200zl").mouseReleaseEvent = \
            lambda event: self.insert_money(200)
        ViewHelper.find_button_clickable_by_object_name("button_500zl").mouseReleaseEvent = \
            lambda event: self.insert_money(500)

    def set_coins_handlers(self):
        """
        Set event handlers for clicking in coins
        :return: nothing
        """
        ViewHelper.find_button_clickable_by_object_name("button_5gr").mouseReleaseEvent = \
            lambda event: self.insert_money(0.05)
        ViewHelper.find_button_clickable_by_object_name("button_10gr").mouseReleaseEvent = \
            lambda event: self.insert_money(0.10)
        ViewHelper.find_button_clickable_by_object_name("button_20gr").mouseReleaseEvent = \
            lambda event: self.insert_money(0.20)
        ViewHelper.find_button_clickable_by_object_name("button_50gr").mouseReleaseEvent = \
            lambda event: self.insert_money(0.50)
        ViewHelper.find_button_clickable_by_object_name("button_1zl").mouseReleaseEvent = \
            lambda event: self.insert_money(1.00)
        ViewHelper.find_button_clickable_by_object_name("button_2zl").mouseReleaseEvent = \
            lambda event: self.insert_money(2.00)
        ViewHelper.find_button_clickable_by_object_name("button_5zl").mouseReleaseEvent = \
            lambda event: self.insert_money(5.00)

    @hide_groupbox_widgets
    def set_screen_by_ticket_status(self, ticket_status, flag=False):
        """
        Change visible screen with widgets
        :param ticket_status: used to determine screen to show
        :param flag: flag to determine if user want to buy different ticket
        :return: nothing
        """
        if ticket_status == TicketBuyStatus.type:
            if flag is True:
                self.api.socket.send(bytes("back", "utf-8"))

            self.back_button.setVisible(False)
            self.button_coins.setVisible(False)
            self.button_banknotes.setVisible(False)
            group_box = ViewHelper.find_group_box_by_object_name("ticket_group_box_type")
            group_box.setGeometry(self.default_geometry)
            group_box.setVisible(True)

        if ticket_status == TicketBuyStatus.zone:
            group_box = ViewHelper.find_group_box_by_object_name("ticket_group_box_zone")
            group_box.setGeometry(self.default_geometry)
            group_box.setVisible(True)

        if ticket_status == TicketBuyStatus.ticket:
            group_box = ViewHelper.find_group_box_by_object_name("ticket_group_box_ticket")
            group_box.setGeometry(self.default_geometry)
            group_box.setVisible(True)

        if ticket_status == TicketBuyStatus.count:
            group_box = ViewHelper.find_group_box_by_object_name("ticket_group_box_count")
            group_box.setGeometry(self.default_geometry)
            group_box.setVisible(True)

        if ticket_status == TicketBuyStatus.sum_up:
            group_box = ViewHelper.find_group_box_by_object_name("ticket_group_box_sum_up")
            group_box.setGeometry(self.default_geometry)
            group_box.setVisible(True)
            self.button_coins.setVisible(True)
            self.button_banknotes.setVisible(True)
            self.back_button.setVisible(True)

        if ticket_status == TicketBuyStatus.finish:
            group_box = ViewHelper.find_group_box_by_object_name("ticket_group_box_finish")
            group_box.setGeometry(self.default_geometry)
            group_box.setVisible(True)
            self.button_coins.setVisible(False)
            self.button_banknotes.setVisible(False)
            self.back_button.setVisible(False)

    @show_widgets(["button_coins", "button_banknotes", "back_button"])
    @hide_widgets(["coins_group_box", "banknote_group_box"])
    def insert_money(self, coin):
        """
        Send inserted coins/banknotes
        :param coin: inserted money
        :return: nothing
        """
        self.api.socket.send(bytes(coin.__str__(), "utf-8"))
        new_total_cost = self.api.socket.recv(1024)

        decoded_new_total_cost = float(new_total_cost.decode("utf-8"))
        label_total_cost = ViewHelper.find_label_by_object_name("label_total_cost")
        label_total_cost.setText(decoded_new_total_cost.__str__())

        if decoded_new_total_cost <= 0:
            self.sum_up_next_step_handler()

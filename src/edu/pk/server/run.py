from injector import Injector

from src.edu.pk.server.ticket_machine import TicketMachine

if __name__ == "__main__":
    """
    Startup file of server class
    """
    injector = Injector()
    ticket_machine = injector.get(TicketMachine)
    ticket_machine.run()

import socket
import requests
import json
import pickle

from injector import inject

from src.edu.pk.server.patterns.factory.ticket_factory import TicketFactory
from src.edu.pk.server.patterns.strategy.strategy import FormatterContext, UpperCaseFormatter, LowerCaseFormatter
from src.edu.pk.server.trolley_service import Trolley


class TicketMachine:
    """
    Ticket machine represents server class (client-server pattern)
    """
    __client_socket = None
    __current_kind_of_ticket = None
    __current_zone = None
    __current_ticket_id = None
    __amount = None

    @inject
    def __init__(self, __trolley: Trolley):
        """
        The constructor initializes the fields, opens a connection and listens to clients
        :param __trolley: client trolley
        """
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((socket.gethostname(), 1234))
        self.socket.listen(5)

        self.__trolley = __trolley
        self.upper_context = FormatterContext()
        self.upper_context.set(UpperCaseFormatter())
        self.lower_context = FormatterContext()
        self.lower_context.set(LowerCaseFormatter())

    def run(self):
        """
        Startup method - accepts client connection and carry out communication
        :return: nothing
        """
        while True:
            self.__client_socket, address = self.socket.accept()
            self.__trolley = Trolley()
            try:
                self.main_communication()
            except ConnectionAbortedError:
                continue

    def main_communication(self):
        """
        Main communication with client
        :return:
        """
        self.ticket_type_handler()
        self.zone_handler()
        self.tickets_handler()
        self.amount_handler()
        message = self.sum_up_handler()
        try:
            self.paid_handler(message)
        except ValueError:
            self.finish_handler(message)

    def ticket_type_handler(self):
        """
        Supports ticket type selection
        :return: nothing
        """
        self.__client_socket.send(bytes("half|full", "utf-8"))
        print(self.upper_context.print("server")
              + self.lower_context.print(" | sent to client ticket type options (half, full)"))
        self.__current_kind_of_ticket = self.__client_socket.recv(1024).decode("utf-8")
        print(self.upper_context.print("client")
              + self.lower_context.print(f" | option selected: {self.__current_kind_of_ticket}"))

    def zone_handler(self):
        """
        Supports zone selection
        :return: nothing
        """
        self.__client_socket.send(bytes("1|2", "utf-8"))
        print(self.upper_context.print("server") + self.lower_context.print(" | sent to client zone options (1, 2)"))
        self.__current_zone = self.__client_socket.recv(1024).decode("utf-8")
        print(self.upper_context.print("client")
              + self.lower_context.print(f" | option selected: {self.__current_zone}"))

    def tickets_handler(self):
        """
        Supports ticket selection
        :return: nothing
        """
        endpoint = "http://127.0.0.1:5000/ticket/" + self.__current_kind_of_ticket.__str__() + "/" \
                   + self.__current_zone.__str__()
        request = requests.get(url=endpoint)
        tickets = json.loads(request.text)
        self.__client_socket.send(bytes(pickle.dumps(tickets)))
        print(self.upper_context.print("server")
              + self.lower_context.print(f" | sent to client possible tickets ({tickets}))"))
        self.__current_ticket_id = self.__client_socket.recv(1024).decode("utf-8")
        print(self.upper_context.print("client")
              + self.lower_context.print(f" | ticket selected: {self.__current_ticket_id}"))

    def amount_handler(self):
        """
        Supports amount selection
        :return: nothing
        """
        endpoint = "http://127.0.0.1:5000/ticket/" + self.__current_ticket_id
        request = requests.get(url=endpoint)
        ticket = json.loads(request.text)
        print(self.upper_context.print("info")
              + self.lower_context.print(" | the ticket with the selected id looks like this:"))
        ticket_factory = TicketFactory.get_ticket(self.__current_kind_of_ticket.__str__(),
                                                  self.bytes_to_int(self.__current_zone), ticket['price'],
                                                  ticket['ticket_type'])
        ticket_factory.print_ticket()
        self.__client_socket.send(bytes("amount", "utf-8"))
        print(self.upper_context.print("server")
              + self.lower_context.print(" | an inquiry was sent for the number of tickets"))
        self.__amount = self.__client_socket.recv(1024).decode("utf-8")
        print(self.upper_context.print("client") + self.lower_context.print(f" | amount selected: {self.__amount}"))
        self.__trolley.insert_ticket(self.__current_ticket_id, self.__amount,
                                     float(ticket['price']) * int(self.__amount))

    @staticmethod
    def bytes_to_int(bytes_to_convert):
        """
        Converts bytes to integer
        :param bytes_to_convert: bytes
        :return: integer
        """
        result = 0
        for byte in bytes_to_convert:
            result = result * 256 + int(byte)
        return result

    def sum_up_handler(self):
        """
        Supports sum up
        :return: nothing
        """
        endpoint = "http://127.0.0.1:5000/ticket"
        request = requests.get(url=endpoint)
        tickets = json.loads(request.text)
        information_about_tickets = ""
        for i in self.__trolley.get_trolley().keys():
            information_about_tickets = information_about_tickets + "["
            information_about_tickets = information_about_tickets + \
                list(filter(lambda ticket: int(i) == int(ticket["_id"]), tickets))[0]['ticket_type'] + " "
            information_about_tickets = information_about_tickets + \
                list(filter(lambda ticket: int(i) == int(ticket["_id"]), tickets))[0]['price'].__str__() + "],"
        self.__client_socket.send(
            bytes(self.__trolley.get_total_cost().__str__() + "|" + information_about_tickets, "utf-8"))
        print(self.upper_context.print("server")
              + self.lower_context.print(f" | sent to client total cost {self.__trolley.get_total_cost().__str__()} "
                                         f"and information about tickets {information_about_tickets}"))
        message = self.__client_socket.recv(1024).decode("utf-8")
        print(self.upper_context.print("client")
              + self.lower_context.print(f" | client sends information about what he wants "
                                         f"to take as the next step: {message}"))
        return message

    def paid_handler(self, message):
        """
        Supports payment
        :param message: data form client
        :return: nothing
        """
        money = float(message)
        self.__trolley.pay_for_the_ticket(money)
        self.__client_socket.send(bytes(self.__trolley.get_total_cost().__str__(), "utf-8"))
        print(self.upper_context.print("server")
              + self.lower_context.print(f" | sent to client total cost {self.__trolley.get_total_cost().__str__()}"))
        self.paid_communication()

    def paid_communication(self):
        """
        Supports paid communication
        :return: nothing
        """
        message = self.__client_socket.recv(1024).decode("utf-8")
        print(self.upper_context.print("client") + self.lower_context.print(f" | client has paid: {message}"))
        try:
            self.paid_handler(message)
        except ValueError:
            self.finish_handler(message)

    def finish_handler(self, message):
        """
        Supports finish step
        :param message: data from client
        :return: nothing
        """
        if message == "next":
            self.__client_socket.send(bytes("Drukuję bilety {'id': 'ilość'}:\n "
                                            + self.__trolley.get_trolley().__str__(), "utf-8"))
            print(self.upper_context.print("server")
                  + self.lower_context.print(" | sent to client information: 'Drukuję bilety {'id': 'ilość'}: "
                                             f"{self.__trolley.get_trolley().__str__()}"))
            self.__client_socket.recv(1024).decode("utf-8")
            print(self.upper_context.print("client") + self.lower_context.print(" | client is waiting for the rest"))
            self.__client_socket.send(bytes(self.__trolley.get_total_cost().__str__(), "utf-8"))
            print(self.upper_context.print("server") + self.lower_context.print(
                f" | rest was spend: {self.__trolley.get_total_cost()}"))
        if message == "back":
            self.main_communication()

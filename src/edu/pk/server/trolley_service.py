class Trolley:
    """
    Trolley of client
    """

    def __init__(self):
        """
        The constructor initializes the fields: trolley and total_cost
        """
        self.__trolley = {}
        self.__total_cost = 0

    def insert_ticket(self, ticket_id, amount, cost):
        """
        Puts the ticket into the trolley
        :param ticket_id: ticket id
        :param amount: amount of tickets
        :param cost: total cost (amount * ticket cost)
        :return: nothing
        """
        self.__trolley[ticket_id] = amount
        self.__total_cost = self.__total_cost + round(cost, 2)

    def get_trolley(self):
        """
        State of trolley - getter
        :return: state of trolley
        """
        return self.__trolley

    def get_total_cost(self):
        """
        Total cost - getter
        :return: total cost rounded to 2
        """
        return round(self.__total_cost, 2)

    def pay_for_the_ticket(self, money):
        """
        Allows you to pay for tickets, subtracts the inserted coins from the total amount to be paid
        :param money: money thrown in
        :return: nothing
        """
        self.__total_cost = self.__total_cost - round(money, 2)

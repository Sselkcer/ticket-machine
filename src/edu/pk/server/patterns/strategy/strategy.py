from abc import abstractmethod


class TextFormatterStrategy:
    """
    Base class of text formatters (strategy pattern)
    """

    @abstractmethod
    def format(self, text):
        """
        General formatting method
        :param text: text to format
        :return: formatted text depending on implementation
        """
        pass


class LowerCaseFormatter(TextFormatterStrategy):
    """
    Lowercase format class
    """

    def format(self, text):
        """
        Converts all letters to lowercase
        :param text: text to format
        :return: lowercase text
        """
        return text.lower()


class UpperCaseFormatter(TextFormatterStrategy):
    """
    Uppercase format class
    """

    def format(self, text):
        """
        Converts all letters to uppercase
        :param text: text to format
        :return: uppercase text
        """
        return text.upper()


class FormatterContext:
    """
    Context of formatter
    """

    __strategy = None

    def set(self, strategy):
        """
        Sets the selected strategy
        :param strategy: strategy
        :return: nothing
        """
        self.__strategy = strategy

    def print(self, text):
        """
        Uses the appropriate formatting method depending on the strategy selected
        :param text: text to format
        :return: formatted text depending on the strategy selected
        """
        return self.__strategy.format(text)

from src.edu.pk.server.patterns.factory.tickets import HalfPriceTicketZone1, HalfPriceTicketZone2, \
    FullPriceTicketZone1, FullPriceTicketZone2


class TicketFactory:
    """
    Ticket factory (factory pattern)
    """

    @staticmethod
    def get_ticket(kind_of_ticket, zone, price, ticket_type):
        """
        Returns the appropriate object depending on the parameters passed
        :param kind_of_ticket: kind of ticket
        :param zone: zone of ticket
        :param price: price of ticket
        :param ticket_type: ticket type
        :return: ticket object with given parameters
        """
        if kind_of_ticket == "half":
            if zone == 1:
                return HalfPriceTicketZone1(price, ticket_type)
            elif zone == 2:
                return HalfPriceTicketZone2(price, ticket_type)
        elif kind_of_ticket == "full":
            if zone == 1:
                return FullPriceTicketZone1(price, ticket_type)
            if zone == 2:
                return FullPriceTicketZone2(price, ticket_type)

from abc import ABCMeta, abstractmethod


class Ticket(metaclass=ABCMeta):
    """
    Base class of tickets
    """

    def __init__(self, price, ticket_type):
        """
        The constructor initializes the fields: price and ticket_type
        :param price: price of ticket
        :param ticket_type: ticket type
        """
        self.price = price
        self.ticket_type = ticket_type

    @staticmethod
    @abstractmethod
    def print_ticket():
        """
        General print method
        :return: depends on implementation
        """
        pass


class HalfPriceTicketZone1(Ticket):
    """
    Half price ticket class with zone 1
    """

    def print_ticket(self):
        """
        Prints half price ticket with zone 1
        :return: nothing
        """
        print("_____TICKET_____")
        print("half price")
        print("zone 1")
        print("price: ", self.price)
        print("type: ", self.ticket_type)


class HalfPriceTicketZone2(Ticket):
    """
    Half price ticket class with zone 2
    """

    def print_ticket(self):
        """
        Prints half price ticket with zone 2
        :return: nothing
        """
        print("_____TICKET_____")
        print("half price")
        print("zone 2")
        print("price: ", self.price)
        print("type: ", self.ticket_type)


class FullPriceTicketZone1(Ticket):
    """
    Full price ticket class with zone 1
    """

    def print_ticket(self):
        """
        Prints full price ticket with zone 1
        :return: nothing
        """
        print("_____TICKET_____")
        print("full price")
        print("zone 1")
        print("price: ", self.price)
        print("type: ", self.ticket_type)


class FullPriceTicketZone2(Ticket):
    """
    Full price ticket class with zone 2
    """

    def print_ticket(self):
        """
        Prints full price ticket with zone 2
        :return: nothing
        """
        print("_____TICKET_____")
        print("full price")
        print("zone 2")
        print("price: ", self.price)
        print("type: ", self.ticket_type)

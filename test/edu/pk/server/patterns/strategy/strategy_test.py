import unittest

from src.edu.pk.server.patterns.strategy.strategy import FormatterContext, UpperCaseFormatter, LowerCaseFormatter


class StrategyTest(unittest.TestCase):

    __upper_context = None
    __lower_context = None

    @classmethod
    def setUpClass(cls):
        cls.__upper_context = FormatterContext()
        cls.__lower_context = FormatterContext()

    def test_lower_case_formatter(self):
        text = "TeXT text TEXt"
        formatted_text = "text text text"

        self.__lower_context.set(LowerCaseFormatter())

        self.assertEqual(self.__lower_context.print(text), formatted_text)

    def test_upper_case_formatter(self):
        text = "tExt TEXT teXT"
        formatted_text = "TEXT TEXT TEXT"

        self.__upper_context.set(UpperCaseFormatter())

        self.assertEqual(self.__upper_context.print(text), formatted_text)


if __name__ == '__main__':
    unittest.main()

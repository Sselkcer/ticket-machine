import unittest

from src.edu.pk.server.patterns.factory.ticket_factory import TicketFactory
from src.edu.pk.server.patterns.factory.tickets import HalfPriceTicketZone1, HalfPriceTicketZone2, \
    FullPriceTicketZone1, FullPriceTicketZone2


class TicketFactoryTest(unittest.TestCase):

    __ticket_factory = None

    @classmethod
    def setUpClass(cls):
        cls.__ticket_factory = TicketFactory()

    def test_half_price_ticket_zone1(self):
        ticket = self.__ticket_factory.get_ticket("half", 1, 2.5, "90m")

        self.assertIsInstance(ticket, HalfPriceTicketZone1)

    def test_half_price_ticket_zone2(self):
        ticket = self.__ticket_factory.get_ticket("half", 2, 5, "1ride")

        self.assertIsInstance(ticket, HalfPriceTicketZone2)

    def test_full_price_ticket_zone1(self):
        ticket = self.__ticket_factory.get_ticket("full", 1, 20, "24h")

        self.assertIsInstance(ticket, FullPriceTicketZone1)

    def test_full_price_ticket_zone2(self):
        ticket = self.__ticket_factory.get_ticket("full", 2, 35.5, "48h")

        self.assertIsInstance(ticket, FullPriceTicketZone2)

    def test_is_not_full_price_ticket_zone2(self):
        ticket = self.__ticket_factory.get_ticket("full", 1, 35.5, "48h")

        self.assertNotIsInstance(ticket, FullPriceTicketZone2)


if __name__ == '__main__':
    unittest.main()

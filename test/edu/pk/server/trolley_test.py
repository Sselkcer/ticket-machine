import unittest

from src.edu.pk.server.trolley_service import Trolley


class TrolleyTest(unittest.TestCase):

    __trolley = None

    def setUp(self):
        self.__trolley = Trolley()

    def test_trolley_state(self):
        trolley_state_static = {1: 5}

        self.__trolley.insert_ticket(1, 5, 5 * 2.5)

        self.assertEqual(self.__trolley.get_trolley(), trolley_state_static)

    def test_trolley_sum(self):
        total_cost = 22.75

        self.__trolley.insert_ticket(2, 7, 7 * 3.25)

        self.assertEqual(self.__trolley.get_total_cost(), total_cost)

    def test_payment(self):
        total_cost = 3 * 10
        coin = 5
        total_cost_after_pay = 25

        self.__trolley.insert_ticket(3, 3, total_cost)
        self.__trolley.pay_for_the_ticket(coin)

        self.assertEqual(self.__trolley.get_total_cost(), total_cost_after_pay)


if __name__ == '__main__':
    unittest.main()

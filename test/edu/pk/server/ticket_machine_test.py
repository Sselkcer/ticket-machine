import unittest

from src.edu.pk.server.ticket_machine import TicketMachine


class TicketMachineTest(unittest.TestCase):

    def test_bytes_to_int(self):
        bytes_variable = b'12'
        int_variable = 12594

        result = TicketMachine.bytes_to_int(bytes_variable)

        self.assertEqual(result, int_variable)


if __name__ == '__main__':
    unittest.main()

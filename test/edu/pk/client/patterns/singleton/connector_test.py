import unittest

from src.edu.pk.client.patterns.singleton.connector import Connector


class ConnectorTest(unittest.TestCase):

    def test_singleton(self):
        instance1 = Connector()
        instance2 = Connector()

        self.assertTrue(instance1 == instance2)

    def test_close_connection(self):
        instance = Connector()

        instance.close_connection()

        self.assertEqual(instance.socket.fileno(), -1)


if __name__ == '__main__':
    unittest.main()

import unittest

from src.edu.pk.api.model.mongo_database import app, MongoDatabase
from src.edu.pk.api.ticket_service import TicketService


class TicketServiceTest(unittest.TestCase):

    __all_tickets = """[{"_id":1.0,"kind":"half","price":1.7,"ticket_type":"20m","zone":1.0},{"_id":2.0,"kind":"half","price":2.3,"ticket_type":"50m/1ride","zone":1.0},{"_id":3.0,"kind":"half","price":3.0,"ticket_type":"90m","zone":1.0},{"_id":4.0,"kind":"half","price":3.6,"ticket_type":"2ride","zone":1.0},{"_id":5.0,"kind":"half","price":7.5,"ticket_type":"24h","zone":1.0},{"_id":6.0,"kind":"half","price":14.0,"ticket_type":"48h","zone":1.0},{"_id":7.0,"kind":"half","price":16.0,"ticket_type":"family per one weekend","zone":1.0},{"_id":8.0,"kind":"half","price":18.0,"ticket_type":"group up to 20 people","zone":1.0},{"_id":9.0,"kind":"half","price":21.0,"ticket_type":"72h","zone":1.0},{"_id":10.0,"kind":"half","price":28.0,"ticket_type":"one week","zone":1.0},{"_id":11.0,"kind":"half","price":1.7,"ticket_type":"20m","zone":2.0},{"_id":12.0,"kind":"half","price":2.3,"ticket_type":"50m/1ride","zone":2.0},{"_id":13.0,"kind":"half","price":3.0,"ticket_type":"90m","zone":2.0},{"_id":14.0,"kind":"half","price":3.8,"ticket_type":"2ride","zone":2.0},{"_id":15.0,"kind":"half","price":10.0,"ticket_type":"24h","zone":2.0},{"_id":16.0,"kind":"half","price":16.0,"ticket_type":"family per one weekend","zone":2.0},{"_id":17.0,"kind":"half","price":23.0,"ticket_type":"group up to 20 people","zone":2.0},{"_id":18.0,"kind":"half","price":34.0,"ticket_type":"one week","zone":2.0},{"_id":19.0,"kind":"full","price":3.4,"ticket_type":"20m","zone":1.0},{"_id":20.0,"kind":"full","price":4.6,"ticket_type":"50m/1ride","zone":1.0},{"_id":21.0,"kind":"full","price":6.0,"ticket_type":"90m","zone":1.0},{"_id":22.0,"kind":"full","price":7.2,"ticket_type":"2ride","zone":1.0},{"_id":23.0,"kind":"full","price":15.0,"ticket_type":"24h","zone":1.0},{"_id":24.0,"kind":"full","price":28.0,"ticket_type":"48h","zone":1.0},{"_id":25.0,"kind":"full","price":16.0,"ticket_type":"family per one weekend","zone":1.0},{"_id":26.0,"kind":"full","price":36.0,"ticket_type":"group up to 20 people","zone":1.0},{"_id":27.0,"kind":"full","price":42.0,"ticket_type":"72h","zone":1.0},{"_id":28.0,"kind":"full","price":56.0,"ticket_type":"one week","zone":1.0},{"_id":29.0,"kind":"full","price":3.4,"ticket_type":"20m","zone":2.0},{"_id":30.0,"kind":"full","price":4.6,"ticket_type":"50m/1ride","zone":2.0},{"_id":31.0,"kind":"full","price":6.0,"ticket_type":"90m","zone":2.0},{"_id":32.0,"kind":"full","price":7.6,"ticket_type":"2ride","zone":2.0},{"_id":33.0,"kind":"full","price":20.0,"ticket_type":"24h","zone":2.0},{"_id":34.0,"kind":"full","price":16.0,"ticket_type":"family per one weekend","zone":2.0},{"_id":35.0,"kind":"full","price":46.0,"ticket_type":"group up to 20 people","zone":2.0},{"_id":36.0,"kind":"full","price":68.0,"ticket_type":"one week","zone":2.0}]"""
    __tickets_by_kind_and_zone = """[{"_id":1.0,"kind":"half","price":1.7,"ticket_type":"20m","zone":1.0},{"_id":2.0,"kind":"half","price":2.3,"ticket_type":"50m/1ride","zone":1.0},{"_id":3.0,"kind":"half","price":3.0,"ticket_type":"90m","zone":1.0},{"_id":4.0,"kind":"half","price":3.6,"ticket_type":"2ride","zone":1.0},{"_id":5.0,"kind":"half","price":7.5,"ticket_type":"24h","zone":1.0},{"_id":6.0,"kind":"half","price":14.0,"ticket_type":"48h","zone":1.0},{"_id":7.0,"kind":"half","price":16.0,"ticket_type":"family per one weekend","zone":1.0},{"_id":8.0,"kind":"half","price":18.0,"ticket_type":"group up to 20 people","zone":1.0},{"_id":9.0,"kind":"half","price":21.0,"ticket_type":"72h","zone":1.0},{"_id":10.0,"kind":"half","price":28.0,"ticket_type":"one week","zone":1.0}]"""
    __ticket_by_id = """{"_id":1.0,"kind":"half","price":1.7,"ticket_type":"20m","zone":1.0}"""
    __mongo_database = None
    __ticket_service = None

    @classmethod
    def setUpClass(cls):
        cls.__mongo_database = MongoDatabase()
        cls.__ticket_service = TicketService(cls.__mongo_database)

    def test_get_all_tickets(self):
        with app.app_context():
            self.assertEqual(self.__all_tickets + "\n", self.__ticket_service.get_all_tickets().get_data(as_text=True))

    def test_get_ticket_by_id(self):
        with app.app_context():
            self.assertEqual(self.__ticket_by_id + "\n",
                             self.__ticket_service.get_ticket_by_id(1).get_data(as_text=True))

    def test_get_tickets_by_kind_and_zone(self):
        with app.app_context():
            self.assertEqual(self.__tickets_by_kind_and_zone + "\n",
                             self.__ticket_service.get_tickets_by_kind_and_zone("half", 1).get_data(as_text=True))


if __name__ == '__main__':
    unittest.main()

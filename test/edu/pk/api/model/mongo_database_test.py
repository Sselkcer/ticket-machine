import unittest

from src.edu.pk.api.model.mongo_database import MongoDatabase


class MongoDatabaseTest(unittest.TestCase):

    __mongo_database = None

    def setUp(self):
        self.__mongo_database = MongoDatabase()

    def test_connection_status(self):
        self.assertEqual(self.__mongo_database.get_connection_status(), True)


if __name__ == '__main__':
    unittest.main()
